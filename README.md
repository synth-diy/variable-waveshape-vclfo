## BOM

Name                      | Value
--------------------------|------
R14, R24                  | 1M
R2, R12, R13, R15, R16    | 220k
R21                       | 180k
R1, R3, R9, R17, R18, R22 | 100k
R19                       | 22k
R25                       | 6k8
R23                       | 1k8
R7, R8, R20               | 1k
R4, R5, R6, R10, R11      | 680
C10, C11                  | 10μ
C4, C5, C6, C7, C8, C9    | 100n
C1                        | 47n
C3                        | 22n
C2                        | 1n
D1, D2                    | 1N4148
D3                        | Bicolor LED
Q1, Q2, Q4                | 2N3906
Q3                        | 2N3904
U1                        | LM324
U2                        | LM358
U3                        | LM13700
